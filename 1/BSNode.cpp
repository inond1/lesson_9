#include "BSNode.h"

using std::cout;
using std::endl;

BSNode::BSNode(std::string data)
{
	this->_data = data;
	this->_left = nullptr;
	this->_right = nullptr;
	this->_count = 1;
}



BSNode::BSNode(const BSNode& other)
{
	this->_data = other.getData();
	if (other._left != nullptr){ this->_left = new BSNode(*other._left); }
	else { this->_left = nullptr; }
	if (other._right != nullptr) { this->_right = new BSNode(*other._right); }
	else { this->_right = nullptr; }
	this->_count = other._count;
	
}

BSNode::~BSNode()
{
	if (this->_left != nullptr) {
		delete this->_left;
	}
	if (this->_right!=nullptr) {
		delete this->_right;
	}
	delete this;
}

void BSNode::insert(std::string value)
{
	if (value != this->_data)
	{
		if (this->_data > value )
		{
			if (this->_left == nullptr)
			{
				this->_left = new BSNode(value);
			}
			else
			{
				this->_left->insert(value);
			}
		}
		else
		{
			if (this->_right == nullptr)
			{
				this->_right = new BSNode(value);
			}
			else
			{
				this->_right->insert(value);
			}
		}
	}
	else
	{
		this->_count++;
	}
}


BSNode& BSNode::operator=(const BSNode& other)
{
	this->_data = other.getData();
	if (this->_left != nullptr) { delete this->_left; }
	if (this->_right != nullptr) { delete this->_right; }
	if (other._left != nullptr)
	{
		this->_left = new BSNode(*other._left);
	}
	else { this->_left = nullptr; }
	if (other._right != nullptr) { this->_right = new BSNode(*other._right); }
	else { this->_right = nullptr; }
	this->_count = other._count;
	return *this;
}

bool BSNode::isLeaf() const
{
	return (this->_left == nullptr && this->_right == nullptr ? true : false);
}

std::string BSNode::getData() const
{
	return this->_data;
}

BSNode* BSNode::getLeft() const
{
	return this->_left;
}

BSNode* BSNode::getRight() const
{
	return this->_right;
}

bool BSNode::search(std::string val) const
{
	if (this->_data == val) return true;
	if (this->isLeaf()) return false;
	if (this->_data > val)
	{
		if (this->_left != nullptr)
		{
			return this->_left->search(val);
		}
		return false;
	}
	
	if(this->_right != nullptr)
	{
		return this->_right->search(val);
	}
	return false;
}

int BSNode::getHeight() const
{
	int right = 0, left = 0;
	if (this->isLeaf())
	{
		return 0;
	}
	if (this->_left != nullptr)
	{
		left = this->_left->getHeight()+1;
	}
	if (this->_right)
	{
		right = this->_right->getHeight() + 1;
	}
	if (left > right)
	{
		return left;
	}
	return right;
}

int BSNode::getDepth(const BSNode& root) const
{
	return root.getCurrNodeDistFromInputNode(this);
}

void BSNode::printNodes() const 
{
	if (this->isLeaf())
	{
		cout << this->_data <<" "<<this->_count<< endl;
	}
	else
	{
		if (this->_left != nullptr) // left
		{
			this->_left->printNodes();
			
		}
		cout << this->_data << " " << this->_count << endl; // root
		if (this->_right != nullptr) // right
		{
			this->_right->printNodes();
		}
	}
}

int BSNode::getCurrNodeDistFromInputNode(const BSNode* node) const
{
	int  i = 0;
	if (this->_data == node->_data) { return 0; }
	else if (this->_data > node->_data)
	{
		if (this->_left != nullptr)
		{
			i = this->_left->getCurrNodeDistFromInputNode(node);
			if (i == -1) return i;
			return i + 1;
		}
		else
		{
			return -1;
		}
	}
	else
	{
		if (this->_right != nullptr)
		{
			i = this->_right->getCurrNodeDistFromInputNode(node);
			if (i == -1) return i;
			return i + 1;
		}
		else
		{
			return -1;
		}
	}
}
