#include "BSNode.h"
#include <fstream>
#include <string>
#include <windows.h>

#include "printTreeToFile.h"
#pragma comment(lib, "printTreeToFile.lib")

using std::cout;
using std::endl;
using std::to_string;

int main()
{
	BSNode* printingTree = new BSNode("1");
	printingTree->insert("1");
	for (int i = 2; i < 6; i++)
	{
		printingTree->insert(to_string(i));
		printingTree->insert(to_string(i));
	}
	printingTree->printNodes();






	BSNode* bs = new BSNode("6");
	bs->insert("2");
	bs->insert("8");
	bs->insert("3");
	bs->insert("5");
	bs->insert("9");
	bs->insert("6");


	cout << "Tree height: " << bs->getHeight() << endl;
	cout << "depth of node with 5 depth: " << bs->getLeft()->getRight()->getRight()->getDepth(*bs) << endl;
	cout << "depth of node with 3 depth: " << bs->getLeft()->getRight()->getDepth(*bs) << endl;


	std::string textTree = "BSTData.txt";
	printTreeToFile(bs, textTree);

	system("BinaryTree.exe");
	system("pause");
	remove(textTree.c_str());
	delete bs;

	return 0;
}

