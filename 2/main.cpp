#pragma once
#include "functions.h"
#include <iostream>
#include <string>


int main() {

	//check char compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<char>('a', 'b') << std::endl;
	std::cout << compare<char>('b', 'a') << std::endl;
	std::cout << compare<char>('c', 'c') << std::endl;

	//check char bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	char charArr[arr_size] = { 'z', 'b', 'c', 'o', 'y' };
	bubbleSort<char>(charArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << charArr[i] << " ";
	}
	std::cout << std::endl;

	//check char printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<char>(charArr, arr_size);
	std::cout << std::endl;

	//check Object compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<Object>(Object(3), Object(5)) << std::endl;
	std::cout << compare<Object>(Object(4), Object(2)) << std::endl;
	std::cout << compare<Object>(Object(2), Object(2)) << std::endl;

	//check Object bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	Object ObjectArr[arr_size] = { Object(1000), Object(2), Object(3), Object(17), Object(50) };
	bubbleSort<Object>(ObjectArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << ObjectArr[i] << std::endl;
	}
	std::cout << std::endl;

	//check Object printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<Object>(ObjectArr, arr_size);
	std::cout << std::endl;

	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;


	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;

	system("pause");
	return 1;
}