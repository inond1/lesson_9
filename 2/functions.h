#pragma once
#include <iostream>

template <class T>
int compare(T a, T b);

template <class T>
void bubbleSort(T* arr, int size);

template <class T>
void printArray(T* arr, int size);

template<class T>
inline int compare(T a, T b)
{
	if (a == b)return 0;
	if (a < b) return 1;
	return -1;
}

template<class T>
inline void bubbleSort(T* arr, int size)
{
	T temp;
	for (int i = 0; i < size - 1; i++)
	{
		for (int j = 0; j < size - i - 1; j++)
			if (compare(arr[j], arr[j+1]) == -1)
			{
				temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
	}
				
}

template<class T>
inline void printArray(T* arr, int size)
{
	for (int i = 0; i < size; i++)
	{
		std::cout << arr[i] << std::endl;
	}
}

struct Object
{
	int _member;
	Object(int i);
	Object() = default;
	bool operator==(const Object& other);
	bool operator<(const Object& other);
	bool operator>(const Object& other);
	Object& operator=(const Object& other);
	friend std::ostream& operator<<(std::ostream& os, const Object& obj);
};



