#include "functions.h"

Object::Object(int i)
{
	this->_member = i;
}

bool Object::operator==(const Object& other)
{
	return this->_member == other._member;
}

bool Object::operator<(const Object& other)
{
	return this->_member < other._member;
}

bool Object::operator>(const Object& other)
{
	return this->_member > other._member;
}

Object& Object::operator=(const Object& other)
{
	this->_member = other._member; 
	return *this;
}


std::ostream& operator<<(std::ostream& os, const Object& obj)
{
	os << obj._member;
	return os;
}
