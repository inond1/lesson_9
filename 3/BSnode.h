#pragma once

#include <string>
#include <iostream>
using std::cout;
using std::endl;

template<class T>
class BSnode
{
public:
	BSnode<T>(T data);
	BSnode<T>(const BSnode& other);

	virtual ~BSnode<T>();

	virtual BSnode<T>* insert(T value);
	BSnode<T>& operator=(const BSnode<T>& other);

	bool isLeaf() const;
	T getData() const;
	BSnode<T>* getLeft() const;
	BSnode<T>* getRight() const;

	bool search(T val) const;

	int getHeight() const;
	int getDepth(const BSnode<T>& root) const;

	void printNodes() const; 

protected:
	T _data;
	BSnode* _left;
	BSnode* _right;

	int _count; 
	int getCurrNodeDistFromInputNode(const BSnode<T>* node) const; 

};

template<class T>
inline BSnode<T>::BSnode(T data)
{
	this->_data = data;
	this->_left = nullptr;
	this->_right = nullptr;
	this->_count = 1;
}

template<class T>
inline BSnode<T>::BSnode(const BSnode& other)
{
	this->_data = other.getData();
	if (other._left != nullptr) { this->_left = new BSnode(*other._left); }
	else { this->_left = nullptr; }
	if (other._right != nullptr) { this->_right = new BSnode(*other._right); }
	else { this->_right = nullptr; }
	this->_count = other._count;
}

template<class T>
inline BSnode<T>::~BSnode()
{
	if (this->_left != nullptr) {
		delete this->_left;
	}
	if (this->_right) {
		delete this->_right;
	}
	delete this;
}

template<class T>
inline BSnode<T>* BSnode<T>::insert(T value)
{
	if (value != this->_data)
	{
		if (this->_data > value)
		{
			if (this->_left == nullptr)
			{
				this->_left = new BSnode(value);
				return this;
			}
			else
			{
				return this->_left->insert(value);
			}
		}
		else
		{
			if (this->_right == nullptr)
			{
				this->_right = new BSnode(value);
				return this;
			}
			else
			{
				return this->_right->insert(value);
			}
		}
	}
	else
	{
		this->_count++;
		return this;
	}
}

template<class T>
inline BSnode<T>& BSnode<T>::operator=(const BSnode<T>& other)
{
	this->_data = other.getData();
	if (this->_left != nullptr) { delete this->_left; }
	if (this->_right != nullptr) { delete this->_right; }
	if (other._left != nullptr) { this->_left = new BSnode(*other._left); }
	else { this->_left = nullptr; }
	if (other._right != nullptr) { this->_right = new BSnode(*other._right); }
	else { this->_right = nullptr; }
	this->_count = other._count;
	return *this;
}

template<class T>
inline bool BSnode<T>::isLeaf() const
{
	return (this->_left == nullptr && this->_right == nullptr ? true : false);
}

template<class T>
inline T BSnode<T>::getData() const
{
	return this->_data;
}

template<class T>
inline BSnode<T>* BSnode<T>::getLeft() const
{
	return this->_left;
}

template<class T>
inline BSnode<T>* BSnode<T>::getRight() const
{
	return this->_right;
}

template<class T>
inline bool BSnode<T>::search(T val) const
{
	if (this->_data == val) return true;
	if (this->isLeaf()) return false;
	if (this->_data > val)
	{
		if (this->_left != nullptr)
		{
			return this->_left->search(val);
		}
		return false;
	}

	if (this->_right != nullptr)
	{
		return this->_right->search(val);
	}
	return false;
}

template<class T>
inline int BSnode<T>::getHeight() const
{
	int right = 0, left = 0;
	if (this->isLeaf())
	{
		return 0;
	}
	if (this->_left != nullptr)
	{
		left = this->_left->getHeight() + 1;
	}
	if (this->_right)
	{
		right = this->_right->getHeight() + 1;
	}
	if (left > right)
	{
		return left;
	}
	return right;
}

template<class T>
inline int BSnode<T>::getDepth(const BSnode<T>& root) const
{
	return root.getCurrNodeDistFromInputNode(this);
}

template<class T>
inline void BSnode<T>::printNodes() const
{
	if (this->isLeaf())
	{
		cout << this->_data << " " << this->_count << endl;
	}
	else
	{
		if (this->_left != nullptr) // left
		{
			this->_left->printNodes();

		}
		cout << this->_data << " " << this->_count << endl; // root
		if (this->_right != nullptr) // right
		{
			this->_right->printNodes();
		}
	}
}

template<class T>
inline int BSnode<T>::getCurrNodeDistFromInputNode(const BSnode<T>* node) const
{
	int  i = 0;
	if (this->_data == node->_data) { return 0; }
	else if (this->_data > node->_data)
	{
		if (this->_left != nullptr)
		{
			i = this->_left->getCurrNodeDistFromInputNode(node);
			if (i == -1) return i;
			return i + 1;
		}
		else
		{
			return -1;
		}
	}
	else
	{
		if (this->_right != nullptr)
		{
			i = this->_right->getCurrNodeDistFromInputNode(node);
			if (i == -1) return i;
			return i + 1;
		}
		else
		{
			return -1;
		}
	}
}
