#include "BSnode.h"
#include <fstream>
#include <string>
#include <windows.h>

#define SIZE 15

using std::string;
using std::cout;
using std::endl;

int main()
{
	int intArr[SIZE] = {50, 1000, 7, 4, 30, 22, 39, 1, 0, -6, 11, 56, 13, 71, 111};
	string stringArr[SIZE] = { "50", "1000", "7", "4", "30", "22", "39", "1", "0", "-6", "11", "56", "13", "71", "111" };
	for (int i = 0; i < SIZE; i++)
	{
		cout << intArr[i] << " ";
	}
	cout << endl;
	for (int i = 0; i < SIZE; i++)
	{
		cout << stringArr[i] << " ";
	}
	cout << endl;

	BSnode<int> intTree(intArr[0]);
	BSnode<string> stringTree(stringArr[0]);
	for (int i = 1; i < SIZE; i++)
	{
		intTree.insert(intArr[i]);
		stringTree.insert(stringArr[i]);
	}
	intTree.printNodes();
	cout << endl;
	stringTree.printNodes();

}




































